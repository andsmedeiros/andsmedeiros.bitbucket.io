'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'transform-admin',
    environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. EMBER_MODULE_UNIFICATION: true
        EMBER_METAL_TRACKED_PROPERTIES: true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      validatorDefaultLocale: 'pt-br'
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    ENV.APP.LOG_TRANSITIONS = true;
    ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    ENV.apiEndpoint = 'http://127.0.0.1:3333/api/v1'
    ENV['ember-simple-auth-token'] = {
      serverTokenEndpoint: `${ENV.apiEndpoint}/login`,
      serverTokenRefreshEndpoint: `${ENV.apiEndpoint}/refresh`,
      tokenPropertyName: 'token',
      refreshTokenPropertyName: 'refreshToken',
      refreshAccessTokens: true,
      tokenExpirationInvalidateSession: true,
      tokenExpireName: 'exp',
      refreshLeeway: 60
    }
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
  }

  return ENV;
};
