import ENV from 'transform-admin/config/environment'

const { apiEndpoint } = ENV

function urlFor(path, _query = {}){
  return `${apiEndpoint}/${path}`
}

export { apiEndpoint, urlFor }
