import Model, { attr, hasMany } from '@ember-data/model'

export default class LicenseModel extends Model {
  @attr('string') status
  @attr('string') code
  @attr('number') allowedActivations
  @attr('number') failedActivations
  @attr('number') deviceId
  @attr('string') comments
  @attr('date') createdAt
  @attr('date') updatedAt
  @attr('string') createdBy
  @hasMany('activation') activations
}
