import Model, { attr } from '@ember-data/model'
import Validatable from 'transform-admin/mixins/model-validator'

class UserModel extends Model.extend(Validatable) {
  @attr('string') name
  @attr('string') email
  @attr('string') password
  @attr('string') passwordConfirmation
  @attr('boolean') admin

  validations = {
    name: {
      presence: true,
      format: { with: /^([a-zA-ZáàãâéêíóôõúüçÁÀÃÂéêíóôõÜÇ]+\s?)+$/gi }
    },
    email: {
      presence: true,
      email: true
    },
    password: {
      presence: true,
      format: { with: /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/g }
    },
    passwordConfirmation: {
      presence: true,
      match: 'password'
    },
    admin: {
      inclusion: [true, false]
    }
  }

}

export default UserModel
