import Model, { attr, belongsTo } from '@ember-data/model'

class ActivationModel extends Model {
  @belongsTo('license') license
  @attr('number') timesPerformed
  @attr('date') createdAt
  @attr('date') updatedAt
  @attr('string') signature
}

export default ActivationModel
