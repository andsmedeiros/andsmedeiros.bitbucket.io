import Route from '@ember/routing/route'
import { inject as service } from '@ember/service'
import AuthenticatedRouteMixin
  from 'ember-simple-auth/mixins/authenticated-route-mixin'

export default class AuthRoute extends Route.extend(AuthenticatedRouteMixin) {
  @service current

  model(){
    return this.current.loadUser()
  }
}
