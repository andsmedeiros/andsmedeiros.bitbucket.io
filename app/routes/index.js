import AuthRoute from 'transform-admin/routes/auth'

export default class IndexRoute extends AuthRoute {
  beforeModel(){
    this.transitionTo('licenses.index')
  }
}
