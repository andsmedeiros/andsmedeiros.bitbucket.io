import Route from '@ember/routing/route'
import  { isPresent } from '@ember/utils'
import { underscore } from '@ember/string'

class LicensesIndexRoute extends Route {

  queryParams = {
    page: { refreshModel: true },
    rowsPerPage: { refreshModel: true },
    sortBy: { refreshModel: true },
    sortDirection: { refreshModel: true }
  }

  model(params){
    if (isPresent(params.sortBy)) {
      params.sortBy = underscore(params.sortBy)
    }

    return this.store.query('license', params)
  }
}

export default LicensesIndexRoute
