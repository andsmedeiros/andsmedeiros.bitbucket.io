import Route from '@ember/routing/route'

class UsersNewRoute extends Route {
  model(){
    return this.store.createRecord('user')
  }
}

export default UsersNewRoute
