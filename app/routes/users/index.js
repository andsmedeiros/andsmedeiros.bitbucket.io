import Route from '@ember/routing/route'

class UsersIndexRoute extends Route {

  model(){
    return this.store.findAll('user')
  }
}

export default UsersIndexRoute
