import AuthRoute from 'transform-admin/routes/auth'
import { inject as service } from '@ember/service'

class AdminRoute extends AuthRoute {

  @service current
  @service flash

  afterModel(_transition){
    if (!this.current.user.admin) {
      this.flash.error('Privilégios insuficientes.')
      this.transitionTo('index')
    }
  }

}

export default AdminRoute
