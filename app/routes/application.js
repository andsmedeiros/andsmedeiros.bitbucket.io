import Route from '@ember/routing/route'
import { action } from '@ember/object'
import { schedule } from '@ember/runloop'
import { inject as service } from '@ember/service'
import ApplicationRouteMixin
  from 'ember-simple-auth/mixins/application-route-mixin'

export default class ApplicationRoute extends Route.extend(ApplicationRouteMixin) {
  @service flash
  @service router

  sessionAuthenticated(){
    this.transitionTo('licenses')
  }

  async sessionInvalidated(){
    window.location.href = this.router.urlFor('login')
  }

  @action
  didTransition(){
    schedule('afterRender', () => this.flash.display())
  }

  @action
  async error(error, _transition){
    this.flash.error(error.toString())
  }
}
