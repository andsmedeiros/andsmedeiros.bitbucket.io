import Service from '@ember/service'

const { stringify, parse } = JSON

class StorageService extends Service {

  get container(){
    return window.sessionStorage
  }

  // Adapted from
  // https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API#Feature-detecting_localStorage
  get available(){
    try {
      const dummy = 'dummy'
      this.container.setItem(dummy, dummy)
      this.container.removeItem(dummy)
      return true
    }catch(error) {
      if(!(error instanceof DOMException)) return false

      const { code, name } = error
      return code === 22 ||
        code === 1014 ||
        name === 'QuotaExceededError' ||
        name === 'NS_ERROR_DOM_QUOTA_REACHED' ||
        (this.container && this.container.length !== 0)
    }
  }

  set(key, value){
    if (this.available) {
      this.container.setItem(key, value)
    }
  }

  get(key){
    if (this.available) {
      return this.container.getItem(key)
    }
  }

  delete(key){
    if (this.available) {
      return this.container.removeItem(key)
    }
  }

  setObject(key, value){
    if (this.available) {
      this.set(key, stringify(value))
    }
  }

  getObject(key){
    if (this.available) {
      return parse(this.get(key))
    }
  }
}

export default  StorageService
