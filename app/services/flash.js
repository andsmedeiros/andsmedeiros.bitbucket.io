import Service from '@ember/service'
import { inject as service } from '@ember/service'
import { tracked } from '@glimmer/tracking'

class FlashService extends Service {
  @service storage

  @tracked entries = []

  queue = []
  id = 0

  constructor(...args){
    super(...args)

    const queue = this.storage.getObject('flash-queue')
    if (queue) {
      this.queue = queue
      this.storage.delete('flash-queue')
    }
  }

  add(level, message){
    this.queue.push({ level, message, id: this.id++ })
    this.storage.setObject('flash-queue', this.queue)
    return this
  }

  display(){
    this.entries = [ ...this.entries, ...this.queue ]
    this.queue = []
    this.storage.delete('flash-queue')
  }

  clearFirst(){
    [ , ...this.entries ] = this.entries
  }

  clear(){
    if (this.entries.length > 0) {
      this.entries = []
    }
  }

  clearAll(){
    this.clear()
    this.queue = []
  }

  success(message){ return this.add('success', message) }
  warning(message){ return this.add('warning', message) }
  error(message)  { return this.add('error',   message) }

}

export default FlashService
