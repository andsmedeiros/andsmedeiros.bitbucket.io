import Service, { inject as service } from '@ember/service'
import { tracked } from '@glimmer/tracking'

class CurrentService extends Service {

  @service store
  @service session
  @tracked user = null

  async loadUser(){
    if (!this.session.isAuthenticated) {
      throw new Error('Session is not authenticated')
    }
    this.user = this.user || await this.store.queryRecord('user', { me: true })
    return this.user
  }

  clearUser(){
    this.user = null
  }

}

export default CurrentService
