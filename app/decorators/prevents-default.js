function preventsDefault(klass, name, descriptor) {
  const target = descriptor.value
  console.assert(typeof target === 'function', 'Can only decorate class member methods')
  descriptor.value = function(event, ...args){
    event.preventDefault()
    return target.apply(this, [event, ...args])
  }
  return descriptor
}

export { preventsDefault }
