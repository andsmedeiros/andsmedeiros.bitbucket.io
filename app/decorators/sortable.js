import { tracked } from '@glimmer/tracking'
import { action } from '@ember/object'

function sortable(klass){
  return class extends klass {
    @tracked sortDirection
    @tracked sortBy

    constructor(...args){
      super(...args)
      this.sortDirection = this.sortDirection || 'desc'
    }

    @action
    sortByField(field){
      if (this.sortBy === field) {
        this.toggleSortDirection()
      } else {
        this.sortBy = field
        this.sortDirection = 'desc'
      }
    }

    @action
    toggleSortDirection(){
      this.sortDirection = this.sortDirection === 'desc' ? 'asc' : 'desc'
    }

    sortCollection(collection){
      const { sortBy: field } = this
      const sorted = collection.sortBy(field)
      return this.sortDirection === 'desc' ? sorted.reverseObjects() : sorted
    }
  }
}

export { sortable }
