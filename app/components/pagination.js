import Component from '@glimmer/component'
import { action } from '@ember/object'

export default class PaginationComponent extends Component {
  get visiblePages(){
    const pages = new Set()
    const { page, last } = this.args
    const possiblePages = [1, 2, page - 1, page, page + 1, last - 1, last]
    possiblePages.filter(page => page > 0 && page <= last)
      .forEach(page => pages.add(page))

    return [ ...pages ]
  }

  @action
  getButtonClasses(page){
    const { page: currentPage, last } = this.args
    const classes = []

    if (
      (page === 2 && currentPage > 4) ||
      (page == currentPage + 1 && last > currentPage + 3)
    ) {
      classes.push("last-of-group")
    }

    if (page === currentPage) {
      classes.push('selected')
    }
    return classes.join(' ')
  }
}
