import Component from '@glimmer/component'

const TIME_FORMAT = 'HH:mm:ss'
const DATE_FORMAT = 'DD/MM/YYYY'
const DATETIME_FORMAT = `${DATE_FORMAT}, ${TIME_FORMAT}`

export default class DateTimeComponent extends Component {

  get value(){
    return this.args.value || Date.now()
  }

  get format(){
    const { hideDate, hideTime } = this.args
    if (hideDate) {
      return TIME_FORMAT
    }else if(hideTime){
      return DATE_FORMAT
    }else{
      return DATETIME_FORMAT
    }
  }
}
