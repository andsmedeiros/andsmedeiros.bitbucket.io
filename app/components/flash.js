import Component from '@glimmer/component'
import { action } from '@ember/object'
import { inject as service } from '@ember/service'
import { later } from '@ember/runloop'

const TIMEOUT = 3000
class FlashComponent extends Component {

  @service flash

  @action
  setClearTimer(){
    later(() => {
      this.flash.clearFirst()
    }, TIMEOUT)
  }

}

export default FlashComponent
