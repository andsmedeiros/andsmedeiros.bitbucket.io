import Component from '@glimmer/component'
import { action } from '@ember/object'
import { inject as service } from '@ember/service'

class MenuComponent extends Component {

  @service session
  @service router
  @service current

  get showIf(){
    return this.session.isAuthenticated
  }

  @action
  async logOut(){
    await this.session.invalidate()
    this.router.transitionTo('login')
  }
}

export default MenuComponent
