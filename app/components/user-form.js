import Component from '@glimmer/component'
import { inject as service } from '@ember/service'
import { action } from '@ember/object'
import { isPresent } from '@ember/utils'
import { preventsDefault } from 'transform-admin/decorators/prevents-default'

class UserFormComponent extends Component {
  @service current
  @service flash

  flashValidationError(){
    this.flash
      .error('Usuário inválido. Corrija os dados de usuário e tente novamente.')
      .display()
  }

  @action
  @preventsDefault
  async save(){
    const { user, validator, afterSave } = this.args
    const isValid = isPresent(validator) ? validator(user) : user.validate()
    if (!isValid) return this.flashValidationError()

    const { isNew } = user
    try {
      await user.save()
    } catch (error) {
      return this.flashValidationError()
    }

    const confirmation = `Usuário ${user.name} ${isNew ? 'criado' : 'atualizado'} com sucesso!`
    this.flash.success(confirmation).display()
    if (isPresent(afterSave)) {
      afterSave(user)
    }
  }

  @action
  rollback(){
    this.args.user.rollbackAttributes()
  }
}

export default UserFormComponent
