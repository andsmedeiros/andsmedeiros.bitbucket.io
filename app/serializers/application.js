import DS from 'ember-data'
import { underscore } from '@ember/string'
import { isPresent } from '@ember/utils'
import { isArray } from '@ember/array'

const { JSONSerializer, EmbeddedRecordsMixin } = DS

class ApplicationSerializer extends JSONSerializer.extend(EmbeddedRecordsMixin) {
  keyForAttribute(key, _method){
    return underscore(key)
  }

  normalizeArrayResponse(_store, primaryModelClass, payload, _id, _requestType){
    const documentHash = {}, includedMap = {}
    let payloadData

    if (isArray(payload)) {
      payloadData = payload
    }else{
      payloadData = payload.data
      delete payload.data
      if (isPresent(Object.keys(payload))) {
        documentHash.meta = payload
      }
    }

    documentHash.data = payloadData.map((recordPayload) => {
      const { data, included } = this.normalize(primaryModelClass, recordPayload)
      if (isPresent(included)) {
        included.forEach(i => includedMap[i.id] = i)
      }
      return data
    })

    const included = Object.values(includedMap)
    if (isPresent(included)) {
      documentHash.included = included
    }

    return documentHash
  }

  extractErrors(_store, _primaryModelClass, payload, _id){
    payload.errors = payload.errors.map(error => ({
        code: error.summary,
        detail: error.message,
        source: {
          pointer: `/data/attributes/${error.field}`
        }
      })
    )

    return super.extractErrors(_store, _primaryModelClass, payload, _id)
  }
}

export default ApplicationSerializer
