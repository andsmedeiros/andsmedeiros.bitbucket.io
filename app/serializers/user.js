import ApplicationSerializer from 'transform-admin/serializers/application'
import { isBlank } from '@ember/utils'

class UserSerializer extends ApplicationSerializer {
  attrs = {
    passwordConfirmation: { serialize: false }
  }

  serialize(...args){
    const json = super.serialize(...args)
    if (isBlank(json.password)) {
      delete json.password
    }
    return json
  }
}

export default UserSerializer
