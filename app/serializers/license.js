import ApplicationSerializer from 'transform-admin/serializers/application'

class LicenseSerializer extends ApplicationSerializer {
  attrs = {
    activations: { deserialize: 'records', serialize: false },
    createdBy: { deserialize: 'records', serialize: false }
  }

  normalize(klass, payload){
    payload.user = payload.user_id
    delete payload.user_id
    return super.normalize(klass, payload)
  }
}

export default LicenseSerializer
