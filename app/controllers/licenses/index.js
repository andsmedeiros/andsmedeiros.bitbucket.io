import Controller from '@ember/controller'
import { inject as service } from '@ember/service'
import { tracked } from '@glimmer/tracking'
import { action } from '@ember/object'
import { sortable } from 'transform-admin/decorators/sortable'

@sortable
class LicensesIndexController extends Controller {
  queryParams = ['page', 'rowsPerPage', 'sortBy', 'sortDirection']
  sortBy = 'createdAt'
  allowedActivations = 3

  @service flash

  @tracked
  page = 1

  @tracked
  rowsPerPage = 15

  get totalRows(){
    return this.model.meta.total
  }

  get lastPage(){
    return this.model.meta.lastPage
  }

  @action
  async generateLicense(){
    const license = await this.store.createRecord('license', {
      allowedActivations: this.allowedActivations,
      comments: this.comments
    })
    try {
      await license.save()
      await this.model.update()
      this.flash.success('Licença gerada com sucesso!')
      this.transitionToRoute('licenses.edit', license)
    } catch (e) {
      this.flash.error('Não foi possível gerar a licença.\n' + e.toString())
    }
    this.flash.display()
  }

}

export default LicensesIndexController
