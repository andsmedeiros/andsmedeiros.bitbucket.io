import Controller from '@ember/controller'
import { inject as service } from '@ember/service'
import { action } from '@ember/object'
import { sortable } from 'transform-admin/decorators/sortable'
import { preventsDefault } from 'transform-admin/decorators/prevents-default'

@sortable
class LicensesEditController extends Controller {
  @service flash

  sortBy = 'createdAt'

  @action
  @preventsDefault
  async save(){
    try {
      await this.model.save()
      this.flash.success('Licença atualizada com sucesso')
    } catch (e) {
      this.flash.error(e.toString())
    }
    this.flash.display()
  }

  @action
  async deleteActivation(activation){
    const { signature } = activation
    const prompt = `Após esta ação, qualquer cliente associado a esta ativação será obrigado a reativar seu aplicativo.\nDeseja mesmo liberar a ativação ${signature}?`
    if (window.confirm(prompt)) {
      try {
        await activation.destroyRecord()
        this.flash.success(`Ativação ${signature} liberada com sucesso.`)
      } catch (e) {
        this.flash.error(e.toString())
      }
      this.flash.display()
    }
  }

  @action
  goBack(){
    this.transitionToRoute('licenses.index')
  }

  get activations(){
    return this.sortCollection(this.model.activations)
  }
}

export default LicensesEditController
