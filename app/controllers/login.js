import Controller from '@ember/controller'
import { action, get } from '@ember/object'
import { inject as service } from '@ember/service'
import { preventsDefault } from 'transform-admin/decorators/prevents-default'

export default class LoginController extends Controller {
  @service session
  @service flash

  @action
  @preventsDefault
  async logIn(){
    const { email, password } = this
    try {
      await this.session.authenticate('authenticator:jwt', { email, password })
    } catch (error) {
      const errors = get(error, 'json.errors') ||
        [{ message: 'Não foi possível realizar a operação solicitada.' }]

      for (const { message } of errors) {
        this.flash.error(message)
      }
      this.flash.display()
    }
  }
}
