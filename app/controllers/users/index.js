import Controller from '@ember/controller'
import { action } from '@ember/object'
import { inject as service } from '@ember/service'
import { sortable } from 'transform-admin/decorators/sortable'

@sortable
class UsersIndexController extends Controller {
  @service current
  @service flash

  sortBy = 'name'
  sortDirection = 'desc'

  get users(){
    return this.sortCollection(this.model)
  }

  @action
  async deleteUser(user){
    const { name } = user
    if (!window.confirm(`Deseja mesmo excluir o usuário ${name}?`)) return
    try {
      await user.destroyRecord()
      this.flash.success(`Usuário ${name} excluído com sucesso.`)
    } catch (error) {
      this.flash.error('Não foi possível excluir o usuário solicitado.\n' + error.toString())
    }
    this.flash.display()
  }

  @action
  newUser(){
    this.transitionToRoute('users.new')
  }
}

export default UsersIndexController
