import Controller from '@ember/controller'
import { inject as service } from '@ember/service'
import { action } from '@ember/object'

class UsersEditController extends Controller {

  @service current

  @action
  validateUser(user){
    return user.validate({
      except: ['password:presence', 'passwordConfirmation:presence']
    })
  }

  @action
  goBack(){
    this.transitionToRoute('users.index')
  }
}

export default UsersEditController
