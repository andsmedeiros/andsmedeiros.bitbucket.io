import Controller from '@ember/controller'
import { action } from '@ember/object'

class UsersNewController extends Controller {
  @action
  goBack(){
    this.transitionToRoute('users.index')
  }
}


export default UsersNewController
