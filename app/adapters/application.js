import DS from 'ember-data'
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin'
import { apiEndpoint } from 'transform-admin/utils/api'

const { RESTAdapter } = DS

class ApplicationAdapter extends RESTAdapter.extend(DataAdapterMixin) {
  host = apiEndpoint
  sendClientIdAsQueryParam = true

  get headers(){
    try {
      const { token } = this.session.data.authenticated
      return { Authorization: `Bearer ${token}` }
    } catch (e) {
      return {}
    }
  }
}

export default ApplicationAdapter
