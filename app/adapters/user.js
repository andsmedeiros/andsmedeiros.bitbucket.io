import ApplicationAdapter from 'transform-admin/adapters/application'

class UserAdapter extends ApplicationAdapter {
  urlForQueryRecord(query, model){
    const { me } = query
    if (me) delete query.me
    const endpoint = super.urlForQueryRecord(query, model)
    return me ? `${endpoint}/me` : endpoint
  }
}

export default UserAdapter
