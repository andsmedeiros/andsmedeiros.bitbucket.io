import DS from 'ember-data'
const { Transform } = DS

class Base64Transform extends Transform {
  deserialize(base64) {
    return atob(base64).split('').map(c => c.codePointAt())
  }

  serialize(array) {
    return btoa(...array)
  }
}

export default Base64Transform
